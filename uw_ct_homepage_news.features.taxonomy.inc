<?php
/**
 * @file
 * uw_ct_homepage_news.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uw_ct_homepage_news_taxonomy_default_vocabularies() {
  return array(
    'homepage_news_category' => array(
      'name' => 'Homepage news category',
      'machine_name' => 'homepage_news_category',
      'description' => 'Taxonomy for news on the homepage.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
