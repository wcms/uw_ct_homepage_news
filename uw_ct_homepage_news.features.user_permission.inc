<?php
/**
 * @file
 * uw_ct_homepage_news.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_homepage_news_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create homepage_news content'.
  $permissions['create homepage_news content'] = array(
    'name' => 'create homepage_news content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any homepage_news content'.
  $permissions['delete any homepage_news content'] = array(
    'name' => 'delete any homepage_news content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own homepage_news content'.
  $permissions['delete own homepage_news content'] = array(
    'name' => 'delete own homepage_news content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any homepage_news content'.
  $permissions['edit any homepage_news content'] = array(
    'name' => 'edit any homepage_news content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own homepage_news content'.
  $permissions['edit own homepage_news content'] = array(
    'name' => 'edit own homepage_news content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter homepage_news revision log entry'.
  $permissions['enter homepage_news revision log entry'] = array(
    'name' => 'enter homepage_news revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_news authored by option'.
  $permissions['override homepage_news authored by option'] = array(
    'name' => 'override homepage_news authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_news authored on option'.
  $permissions['override homepage_news authored on option'] = array(
    'name' => 'override homepage_news authored on option',
    'roles' => array(
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_news promote to front page option'.
  $permissions['override homepage_news promote to front page option'] = array(
    'name' => 'override homepage_news promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_news published option'.
  $permissions['override homepage_news published option'] = array(
    'name' => 'override homepage_news published option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_news revision option'.
  $permissions['override homepage_news revision option'] = array(
    'name' => 'override homepage_news revision option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override homepage_news sticky option'.
  $permissions['override homepage_news sticky option'] = array(
    'name' => 'override homepage_news sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  return $permissions;
}
