<?php
/**
 * @file
 * uw_ct_homepage_news.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_homepage_news_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'homepage_news_context';
  $context->description = 'Displays the homepage_news view';
  $context->tag = 'homepage';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-homepage_news-block' => array(
          'module' => 'views',
          'delta' => 'homepage_news-block',
          'region' => 'news',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Displays the homepage_news view');
  t('homepage');
  $export['homepage_news_context'] = $context;

  return $export;
}
